<?php

use Illuminate\Database\Seeder;
use App\Product;

class ProductsTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i < 400; $i++) { 
		
			
			$array = array("instock", "outofstock");
			//$stock[array_rand($array, 1)];
	    	Product::create([
	            'name' => str_random(8),
	            'price' => 5+$i,
	            'stock' => $array[array_rand($array, 1)],
	            'created_at' => date("Y-m-d H:i:s")
	        ]);
    	}
    }
}
