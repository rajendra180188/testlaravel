<?php

use Illuminate\Database\Seeder;
use App\Order;
class OrdersTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i < 40; $i++) { 
			$array = array("new", "processed");
	    	Order::create([
	            'user_id' => $i,
	            'total_amount' => 2*2+$i,
	            'status' => $array[array_rand($array, 1)],
	            'created_at' => date("Y-m-d H:i:s")
	        ]);
    	}
    }
}
