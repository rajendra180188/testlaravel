<?php

use Illuminate\Database\Seeder;
use App\Customer;
class CustomersTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i < 40; $i++) { 
	    	Customer::create([
	            'name' => str_random(8),
	            'email' => str_random(12).'@mail.com',
	            'created_at' => date("Y-m-d H:i:s")
	        ]);
    	}
    }
}
