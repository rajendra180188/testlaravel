<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Product;
use DataTables;

class ProductController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show the application product.
     *
     */
	 
    public function index()  { 
	
        return view('product.list');
    } 
	
    public function productsList()  { 
	
        return DataTables::of(Product::query())->make(true);
		
	}
}
