<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Customer;
use DataTables;
class CustomerController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     */
    public function index()  { 
	
        return view('customer.list');
    }
	
	
    public function customersList()  { 
	
		$users = Customer::select(["id","name", "email", "created_at"]);
return datatables($users)
->editColumn('created_at', function ($user) {
return $user->created_at->format('d/m/Y');
})
->make(true);
        ///return DataTables::of(Customer::query())->make(true);
	}
}
