<?php

namespace App\Http\Controllers;
//use Illuminate\Http\Request;
use Request;
use App\Order;
use DataTables;

class OrderController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show the application Orders.
     *
     */
    public function index()  { 
	
        return view('order.list');
    }
	
    public function ordersList(Request $request)  { 
		///$name = $request->input('user.name');

	
		/* $ordersdata = Order::select(["orders.*","customers.name"])
	                ->join('customers', 'orders.id', '=', 'customers.id');

	return Datatables::of($ordersdata)->filter(function ($query) use ($request) {

	            if ($request->filled('customers')) {
	                $query->where('customers.name', 'LIKE', "%{$request->get('name')}%");
	            }

	        })->make(true); */
	
	$order = Order::select(["orders.*","customers.name"])
	->Join('customers', 'orders.id', '=', 'customers.id');
	return Datatables::of($order)->filter(function($query) {
           ////$query->where('customers.name', 'LIKE', "%{$name}%");
        })->make(true);

	}
}
