<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/* 
Route::get('/', function () {
    return view('welcome');
});
 */
 
Route::get('/', function () { return view('auth.login'); });
Route::get('/register', function () { return view('auth.register'); });

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/customer', 'CustomerController@index')->name('customer');
///Route::get('/products', 'ProductController@index')->name('products');
Route::get('/orders', 'OrderController@index')->name('orders');


Route::get('products', 'ProductController@index')->name('products');
Route::get('products-data', 'ProductController@productsList')->name('product.data');
 

Route::get('orders', 'OrderController@index')->name('orders');
Route::get('orders-data', 'OrderController@ordersList')->name('order.data');
 

Route::get('customers', 'CustomerController@index')->name('customers');
Route::get('customers-data', 'CustomerController@customersList')->name('customer.data');
 
