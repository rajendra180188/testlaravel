@extends('layouts.app')
@section('title', 'Product')
@section('content')
<link rel="stylesheet" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

<div class="box box-info">
	<div class="panel panel-default">
		<div class="panel-body">Product Listing</div>
	</div>
	<div class="box-body">
		<table class="table table-bordered" id="product-table">
            <thead>
                <tr>
                    <th>SNo.</th>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Stock</th>
                </tr>
            </thead>
        </table>
	</div><!-- /.box-body -->

</div><!-- /.box -->

<script>



$(function() {
    $('#product-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('product.data') !!}',
        columns: [
            { data: 'id', name: 'id' },
            { data: 'name', name: 'name' },
            { data: 'price', name: 'price' },
            { data: 'stock', name: 'stock' }
        ]
    });
});
</script>

@endsection
