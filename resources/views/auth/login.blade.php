@extends('layouts.app_outer')
@section('title', 'Login')
@section('content')

<div class="card bg-light" style="width:50%;margin-top:15%;margin-left:24%">
<article class="card-body mx-auto" style="width: 100%;">
	<h4 class="card-title mt-3 text-center">ADMIN LOGIN</h4>
	
	<form method="POST" id="login" action="{{ route('login') }}">
	@csrf
	
    <div class="form-group">
    	<label>{{ __('E-Mail Address') }}</label>
        <input name="email" class="form-control @error('email') is-invalid @enderror" data-validation="required email" placeholder="Pleader enter email" value="{{ old('email') }}">
		@error('email')
			<span class="invalid-feedback" role="alert">
				<strong>{{ $message }}</strong>
			</span>
		@enderror
	</div> <!-- form-group// -->
   
    <div class="form-group">
    	<label>{{ __('Password') }}</label>
        <input type="password" class="form-control @error('password') is-invalid @enderror" name="password"  data-validation="required"  placeholder="Pleader enter password">
		@error('password')
			<span class="invalid-feedback" role="alert">
				<strong>{{ $message }}</strong>
			</span>
		@enderror
    </div> <!-- form-group// -->
                                      
    <div class="form-group">
        <button type="submit" class="btn btn-primary btn-block"> LOGIN  </button>
    </div> <!-- form-group// -->      
	
	@if (Route::has('password.request'))
		<a class="btn btn-link" href="{{ route('password.request') }}">
			{{ __('Forgot Your Password?') }}
		</a>
	@endif
    <p class="text-center">Create an account? <a href="{{url('register')}}">Register</a> </p>                                                                 
</form>
</article>
</div> <!-- card.// -->
@endsection
