@extends('layouts.app_outer')
@section('title', 'Register')
@section('content')

<div class="card bg-light" style="width:50%;margin-top:4%;margin-left:24%">
<article class="card-body mx-auto" style="width: 100%;">
	<h4 class="card-title mt-3 text-center">Create Account</h4>
	<form method="POST" action="{{ route('register') }}">
    @csrf
	<div class="form-group">
		<label>{{ __('Full Name') }}</label>
        <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" data-validation="required" placeholder="Please enter name">
			@error('name')
				<span class="invalid-feedback" role="alert">
					<strong>{{ $message }}</strong>
				</span>
			@enderror
    </div> <!-- form-group// -->
    <div class="form-group">
    	<label>{{ __('E-Mail Address') }}</label>
        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" data-validation="required email" value="{{ old('email') }}"  placeholder="Please enter email">
			@error('email')
				<span class="invalid-feedback" role="alert">
					<strong>{{ $message }}</strong>
				</span>
			@enderror
    </div> <!-- form-group// -->
   
    <div class="form-group">
    	<label>{{ __('Password') }}</label>
        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password"  data-validation="required" placeholder="Please enter password">

		@error('password')
			<span class="invalid-feedback" role="alert">
				<strong>{{ $message }}</strong>
			</span>
		@enderror
    </div> <!-- form-group// -->
    <div class="form-group">
    	<label>{{ __('Confirm Password') }}</label>
        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" data-validation="required" placeholder="Please enter confirm password">
    </div> <!-- form-group// -->                                      
    <div class="form-group">
        <button type="submit" class="btn btn-primary btn-block"> Create Account  </button>
    </div> <!-- form-group// -->      
    <p class="text-center">Already registered ? <a href="{{url('login')}}">Login</a> </p>                                                                 
</form>
</article>
</div> <!-- card.// -->
@endsection