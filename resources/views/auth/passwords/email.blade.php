@extends('layouts.app_outer')
@section('title', 'Forgot Password')
@section('content')
<div class="card bg-light" style="width:50%;margin-top:15%;margin-left:24%">
<article class="card-body mx-auto" style="width: 100%;">
	<h6 class="card-title mt-3 text-center">FORGOT YOUR PASSWORD</h6>
	
	<form method="POST" action="{{ route('password.email') }}" id="forgotpassword">
	@csrf
    <div class="form-group">
    	<label>{{ __('E-Mail Address') }}</label>
        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="Please enter registered email" data-validation="required email">
		@error('email')
			<span class="invalid-feedback" role="alert">
				<strong>{{ $message }}</strong>
			</span>
		@enderror
	</div> <!-- form-group// -->
                            
    <div class="form-group">
        <button type="submit" class="btn btn-success btn-block"> Send Password Reset Link  </button>
    </div> <!-- form-group// -->      
	<div class="row">
		<div class="col-md-4">
			<a class="btn btn-link" href="{{ route('login') }}">
				{{ __('Login') }}
			</a>
		</div>
		<div class="col-md-8 text-right">
			<a class="btn btn-link " href="{{ route('register') }}">
				{{ __('Register') }}
			</a>
		</div>
	</div>
</form>
</article>
</div> <!-- card.// -->
@endsection