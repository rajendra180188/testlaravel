@extends('layouts.app')
@section('title', 'Customers')
@section('content')
<link rel="stylesheet" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

<div class="box box-info">
	<div class="panel panel-default">
		<div class="panel-body">Customers Listing</div>
	</div>
	<div class="box-body">
		<table class="table table-bordered" id="customer-table">
            <thead>
                <tr>
                    <th>SNo.</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Registered date</th>
                </tr>
            </thead>
        </table>
	</div><!-- /.box-body -->

</div><!-- /.box -->

<script>
$(function() {
    $('#customer-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('customer.data') !!}',
        columns: [
            { data: 'id', name: 'id' },
            { data: 'name', name: 'name' },
            { data: 'email', name: 'email' },
            { data: 'created_at', name: 'created_at' }
        ]
    });
});
</script>

@endsection
