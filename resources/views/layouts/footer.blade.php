<div class="modal fade" id="myModal"></div>
<div class="modal fade" id="ajaxloading"></div>
<div id="snackbar" class="alert"></div>
<footer class="main-footer">
	<div class="pull-right hidden-xs">
	  <b>Powered By </b> <span style="color: #72afd2;">Rajendra Vishwakarma</span>
	</div>
		<strong>Copyright &copy; {{date("Y")}}-2020 .</strong>All rights reserved.
</footer>
<div class="control-sidebar-bg"></div>