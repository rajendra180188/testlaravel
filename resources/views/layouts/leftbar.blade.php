<aside class="main-sidebar">
	<section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
			<div class="pull-left image">
				<img src="{{asset('assets/dist/img/avatar.png')}}" class="img-circle" alt="User Image">
			</div>
			<div class="pull-left info">
				<p>{{ Auth::user()->name }}</p>
				<a href="javascript:void(0);" ><i class="fa fa-circle text-success"></i> Online</a>
			</div>
		</div>
		<!-- sidebar menu: : style can be found in sidebar.less -->
		<ul class="sidebar-menu">
			<li class="treeview active">
				<a href="{{url('home')}}"><i class="fa fa-dashboard"></i><span>Dashboard</span></a>
			</li>
			
			<li class="treeview ">
				<a href="{{route('customer')}}"><i class="fa fa-users"></i> <span>Customers</span></a>
			</li>

			<li class="treeview ">
				<a href="{{route('products')}}"><i class="fa fa-list"></i> <span>Product</span></a>
			</li>

			<li class="treeview ">
				<a href="{{route('orders')}}"><i class="fa fa-ban"></i> <span>Orders</span></a>
			</li>

			<li class="treeview">
				<a href="{{ route('logout') }}" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();"><i class="fa fa-lock"></i> <span>Logout</span> </a>
			
				<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
					@csrf
				</form>
			</li>
		</ul>
    </section>
</aside><!-- Content Wrapper. Contains page content -->