<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		
		<!-- CSRF Token -->
		<meta name="csrf-token" content="{{ csrf_token() }}">

		<title> @yield('title')</title>
		<!-- Tell the browser to be responsive to screen width -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<script src="https://livedemo.mbahcoding.com/assets/jquery/jquery-2.1.4.min.js"></script>

		<link href="{{ asset('assets/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
		<!-- Font Awesome -->
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

		<!-- Ionicons -->
		<link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
		<link href="{{ asset('assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('assets/css/common.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('assets/dist/css/AdminLTE.min.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('assets/dist/css/skins/_all-skins.min.css') }}" rel="stylesheet" type="text/css" />

		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	</head>
	<body class="hold-transition skin-blue sidebar-mini">
		<div class="wrapper">
				
			@include("layouts.header")
			@include("layouts.leftbar")
			<div class="content-wrapper">
			   <!-- Main content -->
				<section class="content">
				  <!-- Main row -->
					<div class="row">
						<!-- Left col -->
						<div class="col-md-12">
							@yield('content')
						</div><!-- /.col -->

					</div><!-- /.row -->

				</section><!-- /.content -->
			</div><!-- /.content-wrapper -->
			@include('layouts.footer')
		</div><!-- ./wrapper -->

<script src="{{ asset('assets/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/plugins/fastclick/fastclick.min.js') }}"></script>
<script src="{{ asset('assets/dist/js/app.min.js') }}"></script>
<script src="{{ asset('assets/dist/js/demo.js') }}"></script>
	  
<script type="text/javascript">
	function trlink(url)
	{  window.open(url,'_blank'); }

		  $(window).scroll(function() {
	   if($(window).scrollTop() + $(window).height() == $(document).height()) {
		   //alert("Bottom Reached!");
	   }
	});
	  
///////------- DELET RECORDS FOR AJAX FUNCTION COMMON FUNCTION ------/////////
function deleterow(table,fieldname,fieldvalue) {
	
	var confirmModal = 
	$('<div class="modal fade">' +        
          '<div class="modal-dialog">' +
          '<div class="modal-content">' +
          '<div class="modal-header">' +
            '<a class="close" data-dismiss="modal" >&times;</a>' +
            '<h3>Confirmation</h3>' +
          '</div>' +

          '<div class="modal-body">' +
            '<p>Are you sure want to delete this records ? </p>' +
          '</div>' +

          '<div class="modal-footer">' +
            '<button class="btn btn-danger btn-sm" data-dismiss="modal">CANCEL</button>' +
            '<button id="okButton" class="btn btn-success btn-sm">CONFIRM </button>' +
          '</div>' +
          '</div>' +
          '</div>' +
        '</div>');
    confirmModal.find('#okButton').click(function(event) {
		
	
		 $.ajax({
        'url' : "http://localhost/project/codeigniter/gst/common/del_single",
        'type' : 'POST',
		'async': 'isAsync',
        'data' : {
            table 	: table,
            fieldname : fieldname,
            fieldvalue 	: fieldvalue,
        },
        'success' : function(res) {
			//alert(res); exit();
		if(res == "ok"){ 
			//$("#"+fieldvalue+"").fadeOut('slow');
			//$(".content").load();
		location.reload();

		}
		else { alert('Error');  }
        },
        'error' : function(request,error)
        {
            alert("Request: "+JSON.stringify(request));
        }
    });
      confirmModal.modal('hide');
    }); 

    confirmModal.modal('show');    
};




  ///////------- CHANGE STATUS FOR COMMON FUNCTION ------/////////
function rowstatus(table,idfield,id,statusfield,status) {

	var confirmModal = 
	$('<div class="modal fade">' +        
          '<div class="modal-dialog">' +
          '<div class="modal-content">' +
          '<div class="modal-header">' +
            '<a class="close" data-dismiss="modal" >&times;</a>' +
            '<h3>Confirmation</h3>' +
          '</div>' +

          '<div class="modal-body">' +
            //'<p>Hi do you want to Change '+ currstatus +' To status '+changestatus+'? </p>' +
			'<p>Are you sure you want to Change  status  ? </p>' +
          '</div>' +

          '<div class="modal-footer">' +
            '<button class="btn btn-danger btn-sm" data-dismiss="modal">CANCEL</button>' +
            '<button id="okButton" class="btn btn-success btn-sm">CONFIRM </button>' +
          '</div>' +
          '</div>' +
          '</div>' +
        '</div>');
		
    confirmModal.find('#okButton').click(function(event) {
		 $.ajax({

        'url' : "http://localhost/project/codeigniter/gst/common/change_status",
        'type' : 'POST',
        'data' : {
            table 			: table,
            idfield 		: idfield,
            id 				: id,
            statusfield 	: statusfield,
            status 			: status,
        },
        'success' : function(res) {
			//alert(res); exit();
		if(res == "ok"){ location.reload(); }
		else { alert('Error');  }
		
        },
        'error' : function(request,error)
        {
            alert("Request: "+JSON.stringify(request));
        }
    });
	
	confirmModal.modal('hide');
    }); 

    confirmModal.modal('show');    
};


 
	</script>
	
<!----- MULTI DELETE OPTION SCRIPT-------->


<script>
 function checkAll(ele) {
     var checkboxes = document.getElementsByTagName('input');
     if (ele.checked) {
         for (var i = 0; i < checkboxes.length; i++) {
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = true;
				 if($(".chkclass")){ $(".del_button").show(); }
             }
         }
     } else {
         for (var i = 0; i < checkboxes.length; i++) {
             console.log(i)
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = false;
				 $(".del_button").hide(); 
             }
         }
     }
 }	
	
	function daleteall(tab,fieldname,refloc){ 
	//alert(tab+" "+refloc);
	if ($('.chkclass:checked').length) {
          var chkId = '';
          $('.chkclass:checked').each(function () {
            chkId += $(this).val() + ",";
          });
          chkId = chkId.slice(0, -1);
		  ///alert(chkId);
		    if (confirm("Are you sure want to delete selected records ?")) {
                confirm_value.value = "Yes";

				$.ajax({
					'url' : "http://localhost/project/codeigniter/gst/admin/common/del_multi",
					'type' : 'POST',
					'async': 'isAsync',
					'data' : {
						tab 		: tab,
						fieldname 	: fieldname,
						fieldvalue 	: chkId,
						refloc	 	: refloc,
					},
					'success' : function(res) {
					//alert(res);
					//if(res == "ok"){ location.reload(); }
					//else { alert('Error');  }
					},
					'error' : function(request,error)
					{
						alert("Request: "+JSON.stringify(request));
					}
				});
			} else { confirm_value.value = "No"; }
			
			
			
        }
        else {
          alert('Please select atleast one ');
        }
	}
</script>
	
<!----- USING DATEPICKER JS ------>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
$( function() {
$( ".datepicker" ).datepicker({ 
	dateFormat: 'dd-mm-yy',
	changeYear: true,
	changeMonth: true,
	yearRange: '-20:+50'
	});
} );

$(".alert").fadeIn('slow').animate({opacity: 2.0}, 1500).fadeOut('slow'); 

</script>
   
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script>
$.validate({
	modules : 'location, date, security, file,toggleDisabled',
	disabledFormFilter : 'form.toggle-disabled',
	showErrorDialogs : false,
	onModulesLoaded : function() {
		$('#country').suggestCountry();
	}
});
  
  
$('form').submit(function (event) {
	if ($(this).hasClass('submitted')) {
		event.preventDefault();
	}
	else {
		//$('.btn').removeClass('btn-info');
		//$('.btn').addClass('btn-success');
		$(this).find(':submit').html('<i class="fa fa-spinner fa-spin"></i> Please wait..');
		$(this).addClass('submitted');
		return
	}
});

////======	LOAD CONTOLLER VIA IN MODEL
function loadmodelbox(location,modeltype,data=""){
	if(location != ""){
		var loadtype = modeltype;
		$.ajax({
			url 	: location,
			type	: "POST",
			data 	:{ id: data ,loadtype: loadtype} ,
			success: function(res)
			{ 
				$('#myModal').modal('show');
				$("#myModal").html(res);
			},
			error: function ( request, errorThrown)
			{ alert("Please reload now");  }
		});
	}
}
$(document).ready(function() {
	$("#myModal").modal({
		show: false,
		backdrop: 'static'
	});
	
});
	
$(document).ajaxStart(function(){ 
	$("button").hide();
	//$('#ajaxloading').model("show"); 
});
$(document).ajaxStop(function(){
	$("button").show();	  
	$('#ajaxProgress').hide(); 
});

function msgerror(msg){
    var x = document.getElementById("snackbar");
    x.className = "show";
	$("#snackbar").addClass("alert-error");
	$("#snackbar").html(msg);
    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
}
	
function msgsuccess(msg){
    var x = document.getElementById("snackbar");
    x.className = "show";
	$("#snackbar").addClass("alert-success");
	$("#snackbar").html(msg);
    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
}	
function msginfo(msg){
    var x = document.getElementById("snackbar");
    x.className = "show";
	$("#snackbar").addClass("alert-info");
	$("#snackbar").html(msg);
    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
}
	
	
</script>
			
  </body>
</html>
