<header class="main-header">
		<!-- Logo -->
		<a href="{{url('home')}}" class="logo">
			<!-- mini logo for sidebar mini 50x50 pixels -->
			<span class="logo-mini"><b>R</b>P</span>
			<!-- logo for regular state and mobile devices -->
			<span class="logo-lg"><b>Rajendra</b>PHP</span>
		</a>
		<!-- Header Navbar: style can be found in header.less -->
		<nav class="navbar navbar-static-top" role="navigation"> 
			<!-- Sidebar toggle button-->
			<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
				<span class="sr-only">Toggle navigation</span>
			</a>
          <!-- Navbar Right Menu -->
			<div class="navbar-custom-menu">
				<ul class="nav navbar-nav">
					<li class="dropdown user user-menu">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<img src="{{asset('assets/dist/img/avatar.png')}}" class="user-image" alt="User Image">  <span class="hidden-xs">{{ Auth::user()->name }}</span>          
						</a>
				
					</li>
             
				</ul> 
			</div> 
		</nav>
	</header>      <!-- Left side column. contains the logo and sidebar -->
      