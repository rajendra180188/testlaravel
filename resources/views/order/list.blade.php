@extends('layouts.app')
@section('title', 'Orders')
@section('content')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
    <script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

<div class="box box-info">
	<div class="panel panel-default">
		<div class="panel-body">Orders Listing</div>
	</div>
	<div class="box-body">
		<table class="table table-bordered" id="order-table">
            <thead>
                <tr>
                    <th>SNo.</th>
                    <th>Customer name</th>
                    <th>Total amount</th>
                    <th>Status</th>
                </tr>
            </thead>
        </table>
	</div><!-- /.box-body -->

</div><!-- /.box -->

<script>



$(function() {
    $('#order-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('order.data') !!}',
        columns: [
            { data: 'id', name: 'id',searchable: false},
            { data: 'name', name: 'name' },
            { data: 'total_amount',searchable: false },
            { data: 'status',searchable: false },
        ]
    });
});
</script>

@endsection
